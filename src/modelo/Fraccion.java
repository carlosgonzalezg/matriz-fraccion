/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;
/**
 *
 * @author Admin
 */
public class Fraccion  implements Comparable{
    
    int numerador;
    int denominador;

    public Fraccion() {
    }

 
    public Fraccion(int numerador, int denominador) throws Exception {
        if(denominador==0){
            throw new Exception  ("No se puede crear un Fraccionario con Cero como Denominador");
        }
        this.numerador = numerador;
        this.denominador = denominador;
    }
    public int getNumerador() {
        return numerador;
    }

    public void setNumerador(int numerador) {
        this.numerador = numerador;
    }

    public int getDenominador() {
        return denominador;
    }

    public void setDenominador(int denominador) {
        this.denominador = denominador;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + this.numerador;
        hash = 89 * hash + this.denominador;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final  Fraccion fraccion2 = (Fraccion) obj;
        return (this.getDecimal()==fraccion2.getDecimal());
        
    }
    
    public void simplificarFraccion(){
      int aux;
      int a=numerador;
      int b=denominador;
      int mcd;
	while (b != 0){
		a %= b;
		aux = b;
		b = a;
		a = aux;
	}
	mcd=a;
        this.numerador/=mcd;
        this.denominador/=mcd;
        
    }
    
    public float getDecimal(){
        return ((float)this.numerador/(float)this.denominador);
       
    }

    @Override
    public String toString() {
        return  this.numerador+"/"+this.denominador;
    }

    @Override
    public int compareTo(Object o) {
              final  Fraccion fraccion2 = (Fraccion) o;
              
              float rta=this.getDecimal()-fraccion2.getDecimal();
              // Version candida-->return (int)(rta);
              if(rta <0)
                  return -1;
              else
                  if (rta>0)
                      return 1;
                   else
                      return 0;
              
    }
    public Fraccion getSumar(Fraccion other){
        Fraccion r = new Fraccion();
        if(this.denominador==other.denominador){
            r.denominador=this.denominador;
            r.numerador=this.numerador+other.numerador;
        }else{
            r.denominador=this.denominador*other.denominador;
            r.numerador=this.numerador*other.denominador+other.numerador*this.denominador;
        }
        return r;
    }
    public Fraccion getRestar(Fraccion other){
        Fraccion r = new Fraccion();
        if(this.denominador==other.denominador){
            r.denominador=this.denominador;
            r.numerador=this.numerador-other.numerador;
        }else{
            r.denominador=this.denominador*other.denominador;
            r.numerador=this.numerador*other.denominador-other.numerador*this.denominador;
        }
        return r;
    }
     public Fraccion getMultiplicar(Fraccion other){
        Fraccion r = new Fraccion();
        
            r.setNumerador(this.numerador*other.numerador);
            r.setDenominador(this.denominador*other.denominador);
           
        
        return r;
    }
    
}
