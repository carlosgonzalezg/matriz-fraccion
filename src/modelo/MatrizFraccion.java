/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class MatrizFraccion implements OperacionMatriz {

    private Fraccion m[][];

    public MatrizFraccion() {
    }

    public MatrizFraccion(String fracciones_excel[][]) throws Exception {
        //Crear la matriz con el tamaño de las filas fracciones_excel, se deja el espacio en blanco para el espacio de las columnas:
        this.m = new Fraccion[fracciones_excel.length][];
        for (int i = 0; i < this.m.length; i++) {
            //Para cada fila de fracciones_excel se debe obtener la cantidad de columnas para esa fila "i" y se crea las columnas:
            int cantColumnas = fracciones_excel[i].length;
            this.m[i] = new Fraccion[cantColumnas];
            /*
     Iterar para almacenar en cada columna una fracción,
    para este paso es necesario que utilice el método split de la clase String, esté método parte la cadena por un carácter
    de tal forma que:
        String algo="3/4";
        String partes[]=algo.split("/");
       El resultado sería un vector de dos posiciones:  partes={"3", "4"};
             */
            for (int j = 0; j < cantColumnas; j++) {
                String parteFraccion[] = fracciones_excel[i][j].split("/");
                //Convertir cada parte de la cadena a su correspondiente entero (Envoltorio):
                int numerador = Integer.parseInt(parteFraccion[0]);
                int denominador = Integer.parseInt(parteFraccion[1]);
                //Crear el objeto fraccción:
                Fraccion nuevaFraccion = new Fraccion(numerador, denominador);
                //Insertar esa nueva fracción a la matriz de fracciones:
                this.m[i][j] = nuevaFraccion;

            }

        }

    }

    @Override
    public String toString() {
        String msg = "";
        for (Fraccion fila[] : this.m) {
            msg += "\n";
            for (Fraccion myF : fila) {
                msg += myF.toString() + " ";
            }
            msg += "\n";
        }

        return msg;

    }

    //Otro toString
    public String toString2() {
        String msg = "";
        for (int i = 0; i < this.m.length; i++) {
            for (int j = 0; j < this.m[i].length; j++) {
                msg += this.m[i][j].toString() + "\t ";
            }
            msg = "\n";
        }

        return msg;

    }

    /**
     * Retorna la menor fracción almacenada en la matriz
     *
     * @return un string con la información de la fracción menor
     */
    public String getMenor() {
        return this.getMenor2().toString();
    }

    private Fraccion getMenor2() {
        Fraccion c = new Fraccion();
        c = this.m[0][0];
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[i].length; j++) {
                if (this.m[i][j].compareTo(c) == -1) {
                    c = m[i][j];
                }
            }
        }
        return c;
    }

    /**
     * Suma dos matrices fraccionarios
     *
     * @param dos matriz de fracción 2
     * @return una matriz con la suma resultante
     */
    private MatrizFraccion getSumar2(MatrizFraccion dos) throws Exception {
        if (!this.verificarDimensiones(dos)) {
            throw new Exception("No se puede sumar ya que las dimensiones de las matrices son distintas");
        } else {
            MatrizFraccion r = new MatrizFraccion();
            r.m = new Fraccion[this.m.length][]; //creo filas
            for (int i = 0; i < this.m.length; i++) {
                r.m[i] = new Fraccion[this.m[i].length];//creo columnas por cada fila
                for (int j = 0; j < this.m[i].length; j++) {
                    r.m[i][j] = this.m[i][j].getSumar(dos.m[i][j]);
                }
            }
            return r;
        }
    }

    /**
     * Suma dos matrices de fracionarios
     *
     * @param dos matriz de fracción 2
     * @return un String que representa el toString() de la matriz de suma
     * resultante
     */
    public String getSumar(MatrizFraccion dos) throws Exception {
        return this.getSumar2(dos).toString();
    }

    /**
     * Resta dos matrices de fracionarios
     *
     * @param dos matriz de fracción 2
     * @return un String que representa el toString() de la matriz de resta
     * resultante
     */
    public String getRestar(MatrizFraccion dos) throws Exception {
        return this.getRestar2(dos).toString();
    }

    private MatrizFraccion getRestar2(MatrizFraccion dos) throws Exception {
        if (!this.verificarDimensiones(dos)) {
            throw new Exception("No se puede restar ya que las dimensiones de las matrices son distintas");
        } else {
            MatrizFraccion r = new MatrizFraccion();
            r.m = new Fraccion[this.m.length][]; //creo filas
            for (int i = 0; i < this.m.length; i++) {
                r.m[i] = new Fraccion[this.m[i].length];//creo columnas por cada fila
                for (int j = 0; j < this.m[i].length; j++) {
                    r.m[i][j] = this.m[i][j].getRestar(dos.m[i][j]);
                }
            }
            return r;
        }
    }

    /**
     * Crea una matriz de 1xM que representa la diagonal principal
     *
     * @return un String con la matriz que almacena la diagonal principal
     */
    public String getDiagonalPrincipal() throws Exception {
        return this.getDiagonalPrincipal1().toString();
    }
    
    /**
     * Obtiene la diagonal principal
     *
     * @return MatrizFraccion de tamaño 1xM
     */
    private MatrizFraccion getDiagonalPrincipal1() throws Exception {
        MatrizFraccion r = null;
        if (this.esMatrizCuadrada()) {
            r = new MatrizFraccion();
            r.m = new Fraccion[1][m.length];
            for (int i = 0; i < m.length; i++) {
                r.m[0][i] = this.m[i][i];
            }

        } else {
            throw new Exception("Error, no es Matriz Cuadrada");
        }
        return r;
    }

    private boolean esMatrizCuadrada() {
        boolean cuadrada = true;
        for (int i = 0; i < this.m.length && cuadrada; i++) {
            if (this.m.length != this.m[i].length) {
                cuadrada = false;
            }
        }

        return cuadrada;
    }

    private boolean esMatrizRectangular() {
        boolean rectangular = !this.esMatrizCuadrada();//verifico que no sea cuadrada, si no lo es queda true
        int columnasPrimeraFila = m[0].length;//obtengo la cantidad de columnas de mi primera fila
        for (int i = 0; i < m.length && rectangular; i++) {
            //verifico que la cantidad de columnas de cada fila sea igual a las columnas de mi primera fila
            if (m[i].length != columnasPrimeraFila) {
                rectangular = false;
            }
        }
        return rectangular;
    }
    //metodo usado para verificar que dos matrices tengan la misma cantidad de columnas en cada fila
    private boolean verificarDimensiones(MatrizFraccion other) {
        boolean tamanio = true;
        if (this.m.length != other.m.length) {//verifico que el tamaño de las filas de ambas matrices sean iguales
            tamanio = false;
        } else {
            for (int i = 0; i < m.length && tamanio; i++) {//por cada fila
                if (this.m[i].length != other.m[i].length) {//verifico que tengan la misma cantidad de columnas
                    tamanio = false;
                }
            }
        }
        return tamanio;
    }

    private MatrizFraccion getTranspuesta2() throws Exception {
        MatrizFraccion n = new MatrizFraccion();
        if (esMatrizRectangular() || esMatrizCuadrada()) {//verifico que no sea dispersa
            n.m = new Fraccion[this.m[0].length][this.m.length];// creo una matriz de fraccionarios de tamaño columnas x filas de this
            for (int i = 0; i < this.m.length; i++) {//recorro filas de this
                for (int j = 0; j < m[i].length; j++) {//recorro columnas de this
                    n.m[j][i] = this.m[i][j]; //agrego a n en la posicion j,i porque intercambio filas por columnas
                }
            }
        } else {
            throw new Exception("Error, no se puede transponer una matriz dispersa");
        }
        return n;
    }

    private void simplificarDos() {
        for (int i = 0; i < this.m.length; i++) {
            for (int j = 0; j < this.m[i].length; j++) {
                this.m[i][j].simplificarFraccion();
            }
        }
    }

    private boolean esDespersa() {
        return !this.esMatrizCuadrada() && !this.esMatrizRectangular();
    }

    public MatrizFraccion multiplyMatrices(MatrizFraccion other) throws Exception{
        if((this.m[0].length==other.m.length)&&(!this.esDespersa()&&!other.esDespersa())){
        int r1=this.m.length;
        int c2=other.m[0].length;
        int c1= this.m[0].length;
        MatrizFraccion product = new MatrizFraccion();
        product.m=new Fraccion[r1][c2];
        for(int i = 0; i < r1; i++) {
            for (int j = 0; j < c2; j++) {
                Fraccion aux=this.m[i][0];
                for (int k = 1; k < c1; k++) {
                    aux=aux.getSumar(this.m[i][k].getMultiplicar(other.m[k][j]));
                }
                product.m[i][j]=aux;
            }
        }

        return product;
        }
        throw new Exception("No se pueden multiuplicar ya que la cantidad de columnas de la matriz 1 no son iguales "
                + "a la cantidad de filas matriz 2, o alguna matriz es dispersa");
    }
    


    @Override
    public String getTranspuesta() throws Exception {
        return this.getTranspuesta2().toString();
    }

    @Override
    public String getMatrizSimplificada() {
        this.simplificarDos();
        return this.toString();
    }

    @Override
    public String getMultiplicación(Object matriz) throws Exception {
        return this.multiplyMatrices((MatrizFraccion) matriz).getMatrizSimplificada();
    }

}
